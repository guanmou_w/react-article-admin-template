import ReactDOM from 'react-dom/client'
// App 根组件
import App from '@/App.tsx'
// 全局样式表
import '@/index.less'

ReactDOM.createRoot(document.getElementById('root')!).render(<App />)
